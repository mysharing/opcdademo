﻿using OPCAutomation;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks.Dataflow;
using System.Transactions;

namespace Yells.Net.OPCDA.Client
{
    //PMX1.0
    //网上的资料都是对一个节点进行操作，下一步就将该节点去除的，且资料不全，下面为我个人所写的 demo, 如有问题，敬请修改 
    //OPCDA 太老了，都没人用了.....
    //需要现将 OPCDAAuto.dll 注册一下
    public class OPCDAClient
    {
        private OPCServer OpcDa { get; set; }
        private readonly string ServerUrl;//对应电脑 IP
        private readonly string ServerIP;//对应的服务器 IP

        private OPCGroups Groups;//组列表
        private OPCGroup Group;//组
        private OPCItems Items;//点位组
        private OPCItem Item;//点位

        /// <summary>
        /// 该服务上所有的节点
        /// </summary>
        public OPCBrowser OpcBrower { get; set; }

        //每一个节点都有对应的数据，将其记录下来，通过其与外界对接，不通过 Items 的 Item
        public List<ItemRecord> ItemServerHandle = new List<ItemRecord>();


        /// 服务端数据改变之后调用
        /// </summary>
        /// <param name="TransactionID">处理ID</param>
        /// <param name="NumItems">项个数</param>
        /// <param name="ClientHandles">OPC客户端的句柄</param>
        /// <param name="ItemValues">节点的值</param>
        /// <param name="Qualities">节点的质量</param>
        /// <param name="TimeStamps">时间戳</param>
        /// 下标一定要从1开始，NumItems 参数是每次事件触发时Group中实际发生数据变化的Item的数量，而不是整个Group里的 Items
        public event DIOPCGroupEvent_DataChangeEventHandler OnDataChange;

        //数据读取之后调用
        public event DIOPCGroupEvent_AsyncReadCompleteEventHandler OnAsyncReadComplete;

        /// <summary>
        /// 创建实例化
        /// </summary>
        /// <param name="serverUrl"></param>
        public OPCDAClient(string serverUrl)
        {
            OpcDa = new OPCServer();
            this.ServerUrl = serverUrl;
        } 

        /// <summary>
        /// 创建实例化
        /// </summary>
        /// <param name="serverUrl"></param>
        public OPCDAClient(string serverUrl, string serverIP)
        {
            OpcDa = new OPCServer();
            this.ServerUrl = serverUrl;
            this.ServerIP = serverIP;
        }

        /// <summary>
        /// 连接对应 OPC 服务的 progId, 或者对应的 Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Connect(string id = null)
        {
            bool isConnect = false;
            IPHostEntry host = Dns.GetHostEntry(ServerUrl);//将主机名或 IP 地址解析为 IPHostEntry 实例。
            try
            {
                //获取到对应 IP 电脑上所有的 OPC 服务器的 progID
                object progIds = OpcDa.GetOPCServers(host.HostName);
                foreach (string progId in (Array)progIds)
                {
                    //没有就全部连接上， 连接对应的 progId
                    if (id == null)
                    {
                        OpcDa.Connect(progId);
                    }
                    else
                    {
                        if (progId == id)
                        {
                            //OpcDa.Connect(progId, ServerIP);
                            OpcDa.Connect(progId);
                        }
                    }

                }
                //将所有点位显示出来
                if (OpcDa.ServerState == Convert.ToInt32(OPCServerState.OPCRunning)) 
                {
                    isConnect = true;
                    OpcBrower = GetAllBrowser();
                }
                else
                {
                    isConnect = false;
                }
            }
            catch (Exception e)
            {
                isConnect = false;
            }
            return isConnect;
        }

        /// <summary>
        /// 订阅需要改变的数据
        /// </summary>
        /// <param name=""></param>
        public void SetSubscribeValue(List<string> ItemIds)
        {
            try
            {
                Groups = OpcDa.OPCGroups;//将服务端的组集合复制到本地
                Group = Groups.Add("MyGroup");//添加一个组
                SetGroupProperty();//设置组属性
                Items = Group.OPCItems;//将组里的节点集合复制到本地节点集合
                Group.DataChange += OnDataChange;
                Group.AsyncReadComplete += OnAsyncReadComplete;

                //订阅需要获取值的节点
                for (int i = 0; i < ItemIds.Count; i++)
                {
                    //后面的 i 即为其客户端句柄
                    Item = Items.AddItem(ItemIds[i], i);
                    
                    //将名称和客户端句柄，服务端句柄，还有类型绑定起来
                    ItemServerHandle.Add(new ItemRecord(Item.ItemID, Item.ClientHandle, Item.ServerHandle, Item.RequestedDataType));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("订阅失败：" + e.Message);
            }
        }

        /// <summary>
        /// 书写对应节点的数据
        /// </summary>
        /// <returns></returns>
        public void WriteItemValue(string itemName, object value)
        {
            foreach (var item in ItemServerHandle)
            {
                if (item.ItemID == itemName)
                {
                    OPCItem bItem = Items.GetOPCItem(item.ServerHandle);
                    //异步写时，是从下标1 开始，而不是从 0, 所以这里第一个数字赋值为 0，下面值同理
                    int[] temp = new int[2] { 0, bItem.ServerHandle };
                    Array serverHandles = (Array)temp;
                    object[] valueTemp = new object[2] { "", value };
                    Array values = (Array)valueTemp;
                    Array Errors;
                    int cancelID;
                    Group.AsyncWrite(1, ref serverHandles, ref values, out Errors, 2009, out cancelID);
                    GC.Collect();
                }
            }
            
            
        }

        /// <summary>
        /// 读取订阅的单个点位函数,读取完会调用读取完成函数，然后这里会获取到对应我读取的值
        /// </summary>
        /// <param name="i"></param>
        public void ReadSingleSubscribeValue(string itemName)
        {
            try
            {
                //异步写时，是从下标1 开始，而不是从 0
                foreach(var item in ItemServerHandle)
                {
                    if (item.ItemID == itemName)
                    {
                        int can;
                        Array Error;
                        int[] serverArr = new int[] { 0, item.ServerHandle };
                        Array serverhandle = (Array)serverArr;
                        //对应的参数分别为读取的数量，读取的服务器句柄，serverhandle这个参数是订阅值的返回值，所以不用管，这里是全部读取。       
                        Group.AsyncRead(ItemServerHandle.Count(), ref serverhandle, out Error, item.ClientHandle, out can);    
                        
                    }

                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine("读取失败");
            }
        }


        /// <summary>
        /// 获取到OPC服务器的所有节点内容
        /// </summary>
        /// <returns>返回OPC服务器的所有节点内容</returns>
        public OPCBrowser GetAllBrowser()
        {
            OPCBrowser OpcBrower = default(OPCBrowser);
            try
            {
                OpcBrower = OpcDa.CreateBrowser();
                OpcBrower.ShowBranches();
                OpcBrower.ShowLeafs(true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return OpcBrower;
        }

        /// <summary>
        /// 设置opc组的函数
        /// </summary>
        private void SetGroupProperty()
        {
            OpcDa.OPCGroups.DefaultGroupIsActive = true;
            OpcDa.OPCGroups.DefaultGroupDeadband = 0;
            Group.UpdateRate = 500;
            Group.IsActive = true;
            Group.IsSubscribed = true;
        }
        /// <summary>
        /// 关闭连接
        /// </summary>
        /// <returns>true:表示关闭</returns>
        public bool CloseConnect()
        {
            bool success = false;
            try
            {
                if (OpcDa != null)
                {
                    OpcDa.Disconnect();
                    success = true;
                }
            }
            catch (Exception e)
            {
                success = false;
                Console.WriteLine(e.Message);
            }

            return success;
        }

        
    }

    /// <summary>
    /// 数据类
    /// </summary>
    /// <param name="ItemID"></param>
    /// <param name="ClientHandle"></param>
    /// <param name="ServerHandle"></param>
    /// <param name="ItemType"></param>
    public record ItemRecord(string ItemID, int ClientHandle, int ServerHandle, short ItemType);
}