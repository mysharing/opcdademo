﻿using Yells.Net.OPCDA.Client;

namespace Test
{
    internal class Program
    {
        public static List<ItemRecord> ItemServerHandles = new List<ItemRecord>();
        static void Main(string[] args)
        {
            
            OPCDAClient oPCDAClient = new OPCDAClient("127.0.0.1");
            oPCDAClient.Connect();
            oPCDAClient.OnDataChange += OPCDAClient_OnDataChange;
            oPCDAClient.OnAsyncReadComplete += OPCDAClient_OnAsyncReadComplete;
            List<string> itemIDs = new List<string>();
            //itemIDs.Add("SG.Test.ttt");
            //itemIDs.Add("SG.Test.ccc");
            //itemIDs.Add("SG.Test.eee");
            //itemIDs.Add("通道 1.sl.SL_3DReturnVal");
            itemIDs.Add("通道 1.sl.kk");
            //itemIDs.Add("通道 1.sl.SL_AlarmConfirm");
            //itemIDs.Add("通道 1.sl.SL_AlarmCommunicate");
            oPCDAClient.SetSubscribeValue(itemIDs);
            ItemServerHandles = oPCDAClient.ItemServerHandle;
            //oPCDAClient.WriteItemValue("SG.Test.ttt", 1);
            //oPCDAClient.WriteItemValue("SG.Test.ccc", 200.0);
            //oPCDAClient.WriteItemValue("SG.Test.eee", '3');
            //oPCDAClient.SetItemValue("通道 1.sl.SL_3DReturnVal", 1);
            oPCDAClient.WriteItemValue("通道 1.sl.kk", 1);
            //oPCDAClient.SetItemValue("通道 1.sl.SL_AlarmConfirm", 1);
            //oPCDAClient.SetItemValue("通道 1.sl.SL_AlarmCommunicate", 1);
            //oPCDAClient.ReadSingleSubscribeValue("通道 1.sl.mm");
            oPCDAClient.ReadSingleSubscribeValue("通道 1.sl.kk");
            while (true)
            {
                Thread.Sleep(100);

            }
        }

        private static void OPCDAClient_OnAsyncReadComplete(int TransactionID, int NumItems, ref Array ClientHandles, ref Array ItemValues, ref Array Qualities, ref Array TimeStamps, ref Array Errors)
        {

        }

        /// <summary>
        /// 数据订阅方法
        /// </summary>
        /// <param name="TransactionID">处理ID</param>
        /// <param name="NumItems">项个数</param>
        /// <param name="ClientHandles">OPC客户端的句柄</param>
        /// <param name="ItemValues">节点的值</param>
        /// <param name="Qualities">节点的质量</param>
        /// <param name="TimeStamps">时间戳</param>
        /// 下标一定要从1开始，NumItems参数是每次事件触发时Group中实际发生数据变化的Item的数量，而不是整个Group里的Items
        private static void OPCDAClient_OnDataChange(int TransactionID, int NumItems, ref Array ClientHandles, ref Array ItemValues, ref Array Qualities, ref Array TimeStamps)
        {
            for (int i = 1; i <= NumItems; i++)
            {
                foreach (var item in ItemServerHandles)
                {
                    if (item.ClientHandle == (int)ClientHandles.GetValue(i))
                    {
                        Console.WriteLine(item.ItemID + "  " + ItemValues.GetValue(i));
                    }
                }
            }
            Console.WriteLine("------------------------");
        }
    }
}