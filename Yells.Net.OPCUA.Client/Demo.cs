﻿using Opc.Ua.Client;
using System.ServiceModel.Channels;

namespace Yells.Net.OPCUA.Client
{
    /*
    OPCUaHelper 源码也在该同一个解决方案文件夹下

    调用 OPCUAHelper Nuget 包实现对 OPC 的调用;下面是演示
     
     */
    public class Demo
    {

        //public async Task YanShiCode()
        //{
        //    ///1.连接OPC UA服务器方法
        //    //实例化操作
        //    OpcUaClient m_OpcUaClient = new OpcUaClient();

        //    ///1.1集中连接方法
        //    //设置匿名连接
        //    m_OpcUaClient.UserIdentity = new UserIdentity( new AnonymousIdentityToken());

        //    //设置用户名连接
        //    m_OpcUaClient.UserIdentity = new UserIdentity( "user", "password" );

        //    //使用证书连接
        //    X509Certificate2 certificate = new X509Certificate2("[证书的路径]", "[密钥]", X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.Exportable);


        //    ///1.2连接服务器
        //    try
        //    {
        //        await m_OpcUaClient.ConnectServer("opc.tcp://192.168.146.137:49321/Kepware.KEPServerEX.V6");
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    ///2.读取OPC UA服务器的节点数据
        //    ///2.1.假设节点字符串： ns=2;s=数据类型示例.16 位设备.R 寄存器.Long4

        //    ///2.2.1.同步单节点数据读取,类型为Int32, 所以我们使用下面的方法读取
        //    try
        //    {
        //        Int32 value = m_OpcUaClient.ReadNode<Int32>("ns=2;s=数据类型示例.16 位设备.R 寄存器.Long4");
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    ///2.2.2.同步批量节点数据读取的操作，分为类型不一致和类型一致两种操作，下面都做个示例
        //    try
        //    {
        //        // 添加所有的读取的节点，此处的示例是类型不一致的情况
        //        List<NodeId> nodeIds = new List<NodeId>();
        //        nodeIds.Add(new NodeId("ns=2;s=数据类型示例.16 位设备.R 寄存器.DWord1"));
        //        nodeIds.Add(new NodeId("ns=2;s=数据类型示例.16 位设备.R 寄存器.Float1"));
        //        nodeIds.Add(new NodeId("ns=2;s=数据类型示例.16 位设备.R 寄存器.LLong4"));

        //        // dataValues按顺序定义的值，每个值里面需要重新判断类型
        //        List<DataValue> dataValues = m_OpcUaClient.ReadNodes(nodeIds.ToArray());

        //        // 如果你批量读取的值的类型都是一样的，比如float，那么有简便的方式
        //        List<string> tags = new List<string>();
        //        tags.Add("ns=2;s=数据类型示例.16 位设备.R 寄存器.Float1");
        //        tags.Add("ns=2;s=数据类型示例.16 位设备.R 寄存器.Float2");
        //        tags.Add("ns=2;s=数据类型示例.16 位设备.R 寄存器.Float3");

        //        // 按照顺序定义的值
        //        List<float> values = m_OpcUaClient.ReadNodes<float>(tags.ToArray());
        //    }
        //    catch (Exception ex)
        //    {
        //    }



        //    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    ///2.3.异步方式读取节点信息

        //    //2.3.1.异步单节点数据读取,类型为Int32, 所以我们使用下面的方法读取
        //    try
        //    {
        //        Int32 value = await m_OpcUaClient.ReadNodeAsync<Int32>("ns=2;s=数据类型示例.16 位设备.R 寄存器.Long4");
        //    }
        //    catch (Exception ex)
        //    {
        //    }

        //    //2.3.2.异步批量节点数据读取的操作，分为类型不一致和类型一致两种操作，下面都做个示例
        //    try
        //    {
        //        // 添加所有的读取的节点，此处的示例是类型不一致的情况
        //        List<NodeId> nodeIds = new List<NodeId>();
        //        nodeIds.Add(new NodeId("ns=2;s=数据类型示例.16 位设备.R 寄存器.DWord1"));
        //        nodeIds.Add(new NodeId("ns=2;s=数据类型示例.16 位设备.R 寄存器.Float1"));
        //        nodeIds.Add(new NodeId("ns=2;s=数据类型示例.16 位设备.R 寄存器.LLong4"));

        //        // dataValues按顺序定义的值，每个值里面需要重新判断类型
        //        List<DataValue> dataValues = await m_OpcUaClient.ReadNodesAsync(nodeIds.ToArray());
        //    }
        //    catch (Exception ex)
        //    {
        //    }

        //    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    ///2.4.订阅方式读取节点信息
        //    ///2.4.1.单节点数据订阅
        //    m_OpcUaClient.AddSubscription("A", "ns=2;s=数据类型示例.16 位设备.R 寄存器.Long4", SubCallback);

        //    ///2.4.2取消单节点数据订阅
        //    m_OpcUaClient.RemoveSubscription("A");

        //    ///2.4.3 多个节点的订阅
        //    string[] MonitorNodeTags = null;
        //    MonitorNodeTags = new string[]
        //    {
        //       "ns=2;s=数据类型示例.16 位设备.R 寄存器.DWord1",
        //       "ns=2;s=数据类型示例.16 位设备.R 寄存器.LLong4",
        //       "ns=2;s=数据类型示例.16 位设备.R 寄存器.Long4",
        //    };
        //    m_OpcUaClient.AddSubscription("B", MonitorNodeTags, SubCallback);


        //    //2.4.4.取消所有节点订阅
        //    m_OpcUaClient.RemoveAllSubscription();
        //    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    ///2.5读取单节点的历史数据
        //    try
        //    {
        //        // 此处演示读取历史数据的操作，读取8月18日12点到13点的数据，如果想要读取成功，该节点是支持历史记录的
        //        List<float> values = m_OpcUaClient.ReadHistoryRawDataValues<float>("ns=2;s=数据类型示例.16 位设备.R 寄存器.Long4",
        //            new DateTime(2021, 5, 1, 12, 0, 0), new DateTime(2021, 2, 25, 13, 0, 0)).ToList();
        //        // 列表数据可用于显示曲线之类的操作

        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //    ///2.6.读取OPC UA服务器中节点信息
        //    ///读取一个节点的关联节点，包含了几个简单的基本信息
        //    try
        //    {
        //        ReferenceDescription[] references = m_OpcUaClient.BrowseNodeReference("ns=2;s=数据类型示例.16 位设备.R 寄存器");
        //        foreach (var item in references)
        //        {
        //            str = string.Format("节点：{0},节点类型：{1},节点名称：{2},节点显示名称：{3}",
        //                             item.NodeId, item.NodeClass, item.BrowseName, item.DisplayName);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }



        //    ///2.7.读取一个节点的相关的所有的属性，主要包含了值，描述，名称，权限等级，等等操作
        //    string str = null;
        //    try
        //    {
        //        OpcNodeAttribute[] nodeAttributes = m_OpcUaClient.ReadNoteAttributes("ns=2;s=数据类型示例.16 位设备.R 寄存器.Long4");
        //        foreach (var item in nodeAttributes)
        //        {
        //            str += string.Format("属性名称：{0},属性类型：{1},属性状态：{2},属性值：{3}",
        //                     item.Name, item.Type, item.StatusCode, item.Value);
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //    }

        //}


        ////2-多节点批量数据订阅回调函数
        //private void SubCallback(string key, MonitoredItem monitoredItem, MonitoredItemNotificationEventArgs args)
        //{
        //    if (InvokeRequired)
        //    {
        //        Invoke(new Action<string, MonitoredItem, MonitoredItemNotificationEventArgs>(SubCallback), key, monitoredItem, args);
        //        return;
        //    }

        //    if (key == "A")
        //    {
        //        // 如果有多个的订阅值都关联了当前的方法，可以通过key和monitoredItem来区分
        //        MonitoredItemNotification notification = args.NotificationValue as MonitoredItemNotification;
        //        if (notification != null)
        //        {
        //            textBox3.Text = notification.Value.WrappedValue.Value.ToString();
        //        }
        //    }
        //    else if (key == "B")
        //    {
        //        // 需要区分出来每个不同的节点信息
        //        MonitoredItemNotification notification = args.NotificationValue as MonitoredItemNotification;
        //        if (monitoredItem.StartNodeId.ToString() == MonitorNodeTags[0])
        //        {
        //            "ns=2;s=数据类型示例.16 位设备.R 寄存器.DWord1" = notification.Value.WrappedValue.Value.ToString();
        //        }
        //        else if (monitoredItem.StartNodeId.ToString() == MonitorNodeTags[1])
        //        {
        //            "ns=2;s=数据类型示例.16 位设备.R 寄存器.LLong4" = notification.Value.WrappedValue.Value.ToString();
        //        }
        //        else if (monitoredItem.StartNodeId.ToString() == MonitorNodeTags[2])
        //        {
        //            "ns=2;s=数据类型示例.16 位设备.R 寄存器.Long4" = notification.Value.WrappedValue.Value.ToString();
        //        }
        //    }
        //}

        ////1-单节点数据订阅的回调函数
        //private void SubCallback(string key, MonitoredItem monitoredItem, MonitoredItemNotificationEventArgs args)
        //{
        //    if (InvokeRequired)
        //    {
        //        Invoke(new Action<string, MonitoredItem, MonitoredItemNotificationEventArgs>(SubCallback), key, monitoredItem, args);
        //        return;
        //    }

        //    if (key == "A")
        //    {
        //        // 如果有多个的订阅值都关联了当前的方法，可以通过key和monitoredItem来区分
        //        MonitoredItemNotification notification = args.NotificationValue as MonitoredItemNotification;
        //        if (notification != null)
        //        {
        //            textBox3.Text = notification.Value.WrappedValue.Value.ToString();
        //        }
        //    }
        //}

    }
}
